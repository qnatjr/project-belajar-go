package cmd

import (
	"fmt"
	"training/project_akhir/handler"
	"training/project_akhir/model"
)

func LastProject() {
	// Login Page
	var loginOption int
	var isLogin bool
	var dataUser model.User

	fmt.Print("1. \t Login \n")
	fmt.Print("2. \t Register \n")
	fmt.Print("Choose your option : ")
	fmt.Scanln(&loginOption)

	switch loginOption {
	case 1:
		profile, err := handler.LoginHandler()
		if !err {
			fmt.Println("Login Failed!")
			break
		}

		dataUser = profile
		isLogin = true
	case 2:
		profile, err := handler.RegisterHandler()
		if err != "" {
			fmt.Println(err)
			fmt.Println("Register Failed!")
			break
		}

		dataUser = profile
		isLogin = true
	default:
		fmt.Println("Internal Server Error")
	}

	if isLogin {
		dataUser.SayHello()
	}
}
