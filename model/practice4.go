package model

type ResultStatistic interface {
	Sum() float64
	Mean() float64
}

type Input struct {
	Data []float64
}

func (l Input) Sum() float64 {
	hasil := sumArray(l.Data...)
	return hasil
}

func (l Input) Mean() float64 {
	var result float64
	for _, numb := range l.Data {
		result += numb
	}
	hasil := result / float64(len(l.Data))
	return hasil
}

// fungsi
func sumArray(numbs ...float64) float64 {
	var result float64 //awal 0
	for _, numb := range numbs {
		result += numb
	}
	return result
}
