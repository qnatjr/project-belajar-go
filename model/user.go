package model

import "fmt"

type User struct {
	ID      uint
	Name    string
	Jabatan string
	Admin   bool
}

func (s *User) Add() {
	s.ID = 2
	s.Admin = false
}

func (s *User) UpdateJabatan(jabatan string) {
	s.Jabatan = jabatan
}

func (s *User) SayHello() {
	fmt.Println("Hello!", s.Name)
}
