package model

type RegisterInput struct {
	Username        string
	Password        string
	ConfirmPassword string
	Jabatan         string
}
