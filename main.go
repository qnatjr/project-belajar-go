package main

import (
	"fmt"
	app "training/project_akhir/cmd"
)

func main() {

	var option int

	fmt.Print("1. \t Project4 \n")
	fmt.Print("2. \t Last Project \n")
	fmt.Print("Choose your option : ")
	fmt.Scanln(&option)

	switch option {
	case 1:
		app.Practice4()
	case 2:
		app.LastProject()
	default:
		fmt.Println("Wrong option")
	}
}
