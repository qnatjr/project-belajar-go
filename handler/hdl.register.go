package handler

import (
	"fmt"
	"training/project_akhir/model"
)

func RegisterHandler() (model.User, string) {
	fmt.Print("\n====================\n")
	fmt.Println("Register Handler")
	fmt.Print("====================\n")

	registerParams := model.RegisterInput{}
	profile := model.User{}

	fmt.Print("Username : ")
	fmt.Scanln(&registerParams.Username)

	if registerParams.Username == "kinat" {
		return profile, "Username is exist"
	}

	fmt.Print("Password : ")
	fmt.Scanln(&registerParams.Password)
	fmt.Print("Confirm Password : ")
	fmt.Scanln(&registerParams.ConfirmPassword)

	if registerParams.Password != registerParams.ConfirmPassword {
		return profile, "Password doesn't match"
	}

	fmt.Print("Jabatan : ")
	fmt.Scanln(&registerParams.Jabatan)

	fmt.Print("====================\n\n")

	profile.Name = registerParams.Username
	profile.Jabatan = registerParams.Jabatan

	return profile, ""
}
