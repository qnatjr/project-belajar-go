package handler

import (
	"fmt"
	"training/project_akhir/model"
)

func LoginHandler() (model.User, bool) {
	fmt.Print("\n====================\n")
	fmt.Println("Login Handler")
	fmt.Print("====================\n")

	loginParams := model.LoginInput{}
	profile := model.User{}

	fmt.Print("Username : ")
	fmt.Scanln(&loginParams.Username)
	fmt.Print("Password : ")
	fmt.Scanln(&loginParams.Password)

	if loginParams.Username != "kinat" && loginParams.Password != "password" {
		return profile, false
	}

	fmt.Print("====================\n\n")

	profile.ID = 1
	profile.Name = "Kinat"
	profile.Jabatan = "Golang Developer"
	profile.Admin = true

	return profile, true
}
