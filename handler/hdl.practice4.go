package handler

import "training/project_akhir/model"

type ResponseStatistic struct {
	NamaData   string
	TotalNilai float64
	Mean       float64
}

func Statistic() ResponseStatistic {
	var data = struct {
		nama  string
		nilai []float64
	}{
		nama:  "Data Fadel",
		nilai: []float64{1, 2, 3, 4, 5, 5, 6, 7, 8, 9, 0},
	}

	var response ResponseStatistic
	var statistik model.ResultStatistic = model.Input{Data: data.nilai}

	response.NamaData = data.nama

	response.TotalNilai = statistik.Sum()
	response.Mean = statistik.Mean()

	return response
}
